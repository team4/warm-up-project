# Warm-Up Project
## Team 4: Kelly, Emily, Tucker, and Drew

### Program Overview 🍿
Our program reads in two datasets about the top 50 movies and their respective directors according to IMDB and allows users to get specific information through certain search queries. The user is prompted to load the data by entering 'load data' and then enter a query about specific information. They may ask about the genre, rating, or director of a movie or about the age, gender,and number of movies made by a director. If the user is confused about the structure of their query, they may enter 'help' to recieve details instructions. Once the user has recived all that they would like to know, they may enter 'quit' to exit the program. If the program is run again the data does not need to be reloaded.

### Development Process 💻
In developing this program the team was split into two sub-groups; half worked on the databasing code and half on the parser (query) code. Drew and Emily took the lead on the databasing end, while Tucker and Kelly managed the parser code. The teams went through a series of revisions, but overall the process went smoothly with the subgroups meeting frequently.
