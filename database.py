# https://www.geeksforgeeks.org/how-to-import-a-csv-file-into-a-sqlite-database-table-using-python
import csv
import sqlite3
from pathlib import Path

def getData():
    create_table = '''CREATE TABLE movies(
                    name TEXT NOT NULL PRIMARY KEY,
                    genre TEXT NOT NULL,
                    score INTEGER NOT NULL,
                    director TEXT NOT NULL);
                    '''
    
    create_table2 = '''
                    CREATE TABLE directors(
                    name TEXT NOT NULL PRIMARY KEY,
                    age INTEGER NOT NULL,
                    gender TEXT NOT NULL,
                    numMovies INTEGER NOT NULL);
                    '''

    # checking if file already exists
    directorsPath = Path("directors.db")
    moviesPath = Path("movies.db")

    if moviesPath.is_file():
        connection = sqlite3.connect('movies.db')
        cursor = connection.cursor()
        
    else:
        connection = sqlite3.connect('movies.db')
        cursor = connection.cursor()
        cursor.execute(create_table)
        file = open('movies.csv')
        contents = csv.reader(file)
        insert_records = "INSERT INTO movies (name, genre, score, director) VALUES(?, ?, ?, ?)"
        cursor.executemany(insert_records, contents)


    if directorsPath.is_file():
        connection2 = sqlite3.connect('directors.db')
        cursor2 = connection2.cursor() 

    else:
        connection2 = sqlite3.connect('directors.db')
        cursor2 = connection2.cursor()   
        cursor2.execute(create_table2)
        file2 = open('directors.csv')
        contents2 = csv.reader(file2)
        insert_records2 = "INSERT INTO directors (name, age, gender, numMovies) VALUES(?, ?, ?, ?)"
        cursor2.executemany(insert_records2, contents2)



    select_all = "SELECT * FROM movies"
    moviesData = cursor.execute(select_all).fetchall()

    select_all2 = "SELECT * FROM directors"
    directorsData = cursor2.execute(select_all2).fetchall()

    connection.commit()
    connection.close()
    connection2.commit()
    connection2.close()

    return moviesData, directorsData
