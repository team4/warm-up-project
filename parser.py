#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 13:13:23 2022

This program prompts the user for a query about information on the top 50 movies of 2018
and their respective directors. The program has a specific set of keywords and structure
of the queries that must be followed to recieve a correct answer. Queries that do not follow 
this structure will be notified and prompted to rephrase their query.

@author: Tucker and Kelly
"""

# Import necessary packages.
from logging import raiseExceptions
import re
import sqlite3 as sql
from database import *
from pathlib import Path


def get_query():
    '''
    
    prompts the user for input and returns a list of the words in their query
    removing unecessary characters. The words are convereted to lowercase to
    better work within the logic.

    '''
    # Prompt user for input.
    query = input("Enter query: ")
    
    # Split query by spaces, convert to lowercase and strip punctuation
    query_list = [q.strip('"?') for q in re.split("( |\\\".*?\\\"|'.*?')", query) if q.strip()] #https://stackoverflow.com/questions/79968/split-a-string-by-spaces-preserving-quoted-substrings-in-python
    
    return query_list

def parse_query():
    
    q = False # Flag variables; terminates program when True
    director_index = ['director', 'age', 'gender', 'num'] # indices for lists to help reduce parsing time
    movie_index = ['movie', 'genre', 'rating', 'director']
    data_loaded = False

    # check to see if the data is already loaded
    directorsPath = Path("directors.db")
    if directorsPath.is_file():
        movies, directors = getData()
        data_loaded = True

    print("Welcome to our program! Using this program you can find out the genre, rating, and director of a movie as well as the age, gender, and numer of movies for each director.\nBefore beginning, please enter 'load data'. \nEnter 'help' to get a list of commands.\n")
    
    while q == False:
        query_list = get_query() # Get query list
        found = False
        try:
            
            # special commands
            if query_list == ['quit']:
                break
            elif query_list == ['help']:
                print("For the below commands, the first word entered is what you want to find out. i.e. 'age director \"Quentin Tarantino\"' would print 58.\nAdditionally, NAME represents the director name or movie title which must be entered in double-qoutes and properly spelled and punctuated. Please user lowercase letters for all other aspects of the query.\\nThese are the possible commands: \nage director NAME\ngender director NAME\nnum director NAME\ngenre movie NAME\nrating movie NAME\ndirector movie NAME\ngender director movie NAME\nage director movie NAME\nnum director movie NAME\nhow many movies?\nhow many directors?\nquit\n")

            elif query_list == ['load', 'data']:
                if data_loaded == False:
                    print("loading data...")
                    data_loaded = True
                    movies, directors = getData()
                else:
                    print("Data already loaded")
            elif query_list == ['how', 'many', 'directors']:
                print(len(directors)-1)
            elif query_list == ['how', 'many', 'movies']:
                print(len(movies)-1)
                
            # for queries of length 3
            elif len(query_list) == 3:
                name = query_list[-1]
                answer = query_list[0]
                
                # director details
                if query_list[-2] == 'director':
                    for obs in directors:
                        if name in obs:
                            i = director_index.index(answer)
                            found = True
                            print(obs[i])
                            
                # movies details
                elif query_list[-2] == 'movie':
                    for obs in movies:
                        if name in obs:
                            i = movie_index.index(answer)
                            found = True
                            print(obs[i])

                else:
                    raise

                # If the name hasn't been found throw an exception
                if found == False:
                    raise
            
            # for queries of length 4
            elif len(query_list) == 4:
                
                # get director name from movie list after initializing director as an empty string
                director = ""
                for obs in movies:
                    name = query_list[-1]
                    answer = query_list[0]
                    if name in obs:
                        director = obs[-1]
                        found = True
                        break
                    
                # then get detail from that director's sub-list
                for obs in directors:
                    if director in obs:
                        i = director_index.index(answer)
                        print(obs[i])

                # If the name hasn't been found throw an exception
                if found == False:
                    raise
            else:
                raise

        except:
            if data_loaded == False:
                print('Data has not been loaded yet. Please enter \'load data\' before entering a query.')
            else:
                print("Please follow the query guidelines (enter HELP to view) and ensure there are no mispellings in your query!")
        
        
parse_query()
